/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.TestsRules;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class VipClientTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <VipClient Class> Tests......");
    }

    @Before
    public void orders() {
        javier.addToOrder("javibarbe");
    }


    @Test
    public void getOrder() {


        assertEquals(1, javier.getOrder().size());
        javier.addToOrder("javibarbe");
        javier.addToOrder("javibarbe");
        javier.addToOrder("javibarbe");
        assertEquals(4, javier.getOrder().size());

        assertArrayEquals(javier.getOrder().toArray(), javier.getDeliveryList().toArray());



    }

    @Test
    public void getPrice() {
        float price = 0f;
        for (Product p :
                javier.getOrder()) {
            price += p.getPrice();
        }

        assertEquals(price, javier.getPrice(), 0.02f);
        assertNotEquals(50 * price, javier.getPrice(), 0.002f);
    }

    @Test

    public void makeOrder() {
        int ords = javier.getNumberOfOrders();
        try {
            javier.makeOrder();
            assertTrue(javier.getDeliveryList().isEmpty());
            javier.makeOrder();
            fail("Expected to catch a VoidOrderException");
        } catch (VoidOrderException voe) {
            assertThat(voe.getMessage(), is("No products in your order"));
        }

        assertEquals(ords + 1, javier.getNumberOfOrders());
    }

    @Test
    public void makeComment() {
        javier.makeComment(javier.findProduct("javibarbe"));
        assertEquals(1, javier.getComments().size());
        assertEquals(4, javier.getComments().get(0).getRating());//rating
        assertEquals("​I really like this product", javier.getComments().get(0).getComment());
        javier.makeComment(javier.findProduct("javibarber"));
        assertNotEquals(2, javier.getComments().size());
    }

    @After
    public void cleanOrders() {
        javier.getDeliveryList().clear();
    }
}