/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.TestsRules;
import dproducts.comments.Comment;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.LinkedList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class StandardClientTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <StandardClient Class> Tests......");
    }

    @Before
    public void orders() {
        jose.addToOrder("josenint");
        jose.addToOrder("josebarbe");


    }

    @Test
    public void getOrder() {
        assertEquals(2, jose.getOrder().size());
        jose.addToOrder("josenint");
        jose.addToOrder("josenint");
        jose.addToOrder("josenint");
        assertEquals(2, jose.getOrder().size());

        assertArrayEquals(jose.getOrder().toArray(),
                new LinkedList<Product>(jose.getDeliveryList()).
                        subList(0, jose.getOrder().size()).toArray());

        jose.getDeliveryList().clear();
        jose.addToOrder("josebarbe");
        assertEquals(1, jose.getOrder().size());


      }

    @Test
    public void getPrice() {
        assertNotEquals(360f, jose.getPrice(), 0.02f);
        float price = jose.getOrder().get(0).getPrice() + jose.getOrder().get(1).getPrice();
        assertEquals(50 * price, jose.getPrice(), 0.002f);
    }

    @Test
    public void makeOrder() {
        try {
            jose.makeOrder();
            assertTrue(jose.getDeliveryList().isEmpty());
            jose.makeOrder();
            fail("Expected to catch a VoidOrderException");
        } catch (VoidOrderException voe) {
            assertThat(voe.getMessage(), is("No products in your order"));
        }
    }

    @Test
    public void makeComment() {
        jose.makeComment(jose.findProduct("josenint"));
        assertEquals(1, jose.getComments().size());
        assertEquals((jose.findProduct("josenint").getName().length() % 5) + 1, jose.getComments().get(0).getRating());//rating
        assertEquals(Comment.DEFAULT_COMMENTS[(jose.findProduct("josenint").getName().length() % 5)], jose.getComments().get(0).getComment());
        jose.makeComment(jose.findProduct("josenint"));
        assertNotEquals(2, jose.getComments().size());
    }

    @After
    public void cleanOrders() {

        jose.getDeliveryList().clear();
    }
}