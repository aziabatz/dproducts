/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.TestsRules;
import dproducts.exceptions.AlreadyCommentedProductException;
import dproducts.exceptions.CannotCommentProductException;
import dproducts.exceptions.NotInFavoritesException;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Food;
import dproducts.products.Home;
import dproducts.products.Product;
import dproducts.products.features.Likeable;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ClientTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <Client Class> Tests......");
    }

    @Test
    public void testToString() {
        assertEquals("1 Rafa Nadal 32 Manacor", rafa.toString());
        assertNotEquals("1 Carolina Marín 25 Huelva", carolina.toString());

    }

    @Test
    public void addProduct() {
        rafa.getFavoriteProducts().clear();
        rafa.addProduct("rafanint", 1);
        assertEquals(1, rafa.getFavoriteProducts().size());
        rafa.addProduct("no existe", 12432);
        assertEquals(1, rafa.getFavoriteProducts().size());
    }

    @Test
    public void findProduct() {
        assertEquals(product1, rafa.findProduct("rafanint"));
        assertNull(rafa.findProduct("rabox"));
    }

    @Test
    public void removeProduct() {
        rafa.removeProduct("rafanint");
        assertNull(rafa.findProduct("rafanint"));
        rafa.removeProduct("rafaxbox");
        assertNull(rafa.findProduct("rafaxbox"));
    }

    @Test
    public void addComment() {
        try {
            ana.addComment("try", ana.findProduct("anamilk"), 3);

            fail("Expected CannotCommentProductException exceptions to be thrown");
        } catch (CannotCommentProductException ccpe) {
            System.out.println("[OK]CannotCommentProductException");
            assertThat(ccpe.getMessage(), is("Food products can't be commented."));
        } catch (NotInFavoritesException nife) {
            assertThat(nife.getMessage(), is("Not in favorites list"));
        } catch (AlreadyCommentedProductException acpe) {
            assertThat(acpe.getMessage(), is("You have already commented this product"));
        }

        try {
            ana.addComment("try", ana.findProduct("ananintnnnn"), 3);
            fail("Expected NotInFavoritesException exceptions to be thrown");
        } catch (CannotCommentProductException ccpe) {
            assertThat(ccpe.getMessage(), is("Food products can't be commented."));
        } catch (NotInFavoritesException nife) {
            System.out.println("[OK]NotInFavoritesException");
            assertThat(nife.getMessage(), is("Not in favorites list"));
        } catch (AlreadyCommentedProductException acpe) {
            assertThat(acpe.getMessage(), is("You have already commented this product"));
        }

        try {
            ana.addComment("try", ana.findProduct("anatowel"), 3);
            ana.addComment("try", ana.findProduct("anatowel"), 3);
            fail("Expected AlreadyCommentedProductException exceptions to be thrown");
        } catch (CannotCommentProductException ccpe) {
            assertThat(ccpe.getMessage(), is("Food products can't be commented."));
        } catch (NotInFavoritesException nife) {
            assertThat(nife.getMessage(), is("Not in favorites list"));
        } catch (AlreadyCommentedProductException acpe) {
            System.out.println("[OK]AlreadyCommentedProductException");
            assertThat(acpe.getMessage(), is("You have already commented this product"));
        }
    }

    @Test
    public void addToOrder() {
                rafa.addToOrder("rafanint");
                rafa.addToOrder("rafanint");
                rafa.addToOrder("rafanint");
        assertTrue(rafa.getDeliveryList().size() == 3);
        assertTrue(rafa.getDeliveryList().contains(rafa.findProduct("rafanint")));
                carolina.addToOrder("carolamp");
        assertTrue(carolina.getDeliveryList().contains(product7));
        assertEquals(product7, carolina.getDeliveryList().peek());
    }


    @Test
    public void makeOrder() throws VoidOrderException {
        float rafaMoney = rafa.getSpentMoney();
        float anaMoney = rafa.getSpentMoney();
        int rafaOrd = rafa.getNumberOfOrders();
        int anaOrd = ana.getNumberOfOrders();

        try {
            rafa.makeOrder();
            carolina.makeOrder();
            fail("Expected an VoidOrderException to be thrown");
        } catch (VoidOrderException voe) {
            assertThat(voe.getMessage(), is("No products in your order"));
        }

        assertEquals(rafaMoney, rafa.getSpentMoney(), 0.0001f);
        assertEquals(rafaOrd, rafa.getNumberOfOrders());

        rafa.addToOrder("rafanint");
        rafa.addToOrder("rafaxbox");
        rafa.makeOrder();

        assertEquals((50 * rafa.findProduct("rafanint").getPrice() +
                        50 * rafa.findProduct("rafaxbox").getPrice()) + rafaMoney,
                rafa.getSpentMoney(), 0.001f);
        assertEquals(rafaOrd + 2, rafa.getNumberOfOrders());

        assertEquals(anaMoney, ana.getSpentMoney(), 0.0001f);
        assertEquals(anaOrd, ana.getNumberOfOrders());

        ana.addToOrder("anamilk");
        ana.addToOrder("anaxbox");
        ana.makeOrder();
        assertEquals(anaMoney + (ana.findProduct("anamilk").getPrice() +
                        ana.findProduct("anaxbox").getPrice()),
                ana.getSpentMoney(), 0.001f);


        assertEquals(anaOrd + 1, ana.getNumberOfOrders());
    }

    @Test
    public void rateProduct() {
        Likeable queso = new Food(23, "Camembert Cheese", 30, 20, 0.8f, "DECEMBER");
        dp.addProduct((Product) queso);
        jose.addProduct("cheese", 23);
        jose.rateProduct((Product) queso, 4);
        assertEquals(1, queso.getLikes());
        jose.rateProduct((Product) queso, 2);
        assertEquals(2, queso.getLikes());
    }

    @Test
    public void tryToComment() {
        Likeable queso = new Food(23, "Camebert Cheese", 30, 20, 0.8f, "DECEMBER");
        Likeable home = new Home(33, "home", 30, 2, 40f, 2);
        dp.addProduct((Product) queso);
        dp.addProduct((Product) home);

        jose.addProduct("cheese", 23);

        jose.tryToComment("Comment", jose.findProduct("cheese"), 3);
        assertEquals(0, jose.getComments().size());

        jose.tryToComment("Comment", jose.findProduct("home"), 3);

        assertEquals(0, jose.getComments().size());
        jose.addProduct("home", 33);
        jose.tryToComment("Comment", jose.findProduct("home"), 3);
        assertEquals(1, jose.getComments().size());

    }
}