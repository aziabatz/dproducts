/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts;

import dproducts.clients.Client;
import dproducts.clients.StandardClient;
import dproducts.clients.VipClient;
import dproducts.products.Entertainment;
import dproducts.products.Food;
import dproducts.products.Home;
import dproducts.products.Product;
import org.junit.Before;

public class TestsRules {

    protected StockManager dp;
    protected Client rafa, carolina, jose, laia, javier, ana;
    protected Product product1, product2, product3, product4, product5, product6;
    protected Product product7, product8, product9, product10, product11, product12, product13, product14;
    protected Product product15, product16, product17, product18, product19, product20;


    public TestsRules() {
        this.setUp();
    }

    private void addClients() {
        rafa = new StandardClient("1", "Manacor", "Rafa Nadal", 32);
        carolina = new VipClient("2", "Huelva", "Carolina Marín", 25);
        jose = new StandardClient("3", "Villanueva de la Serena", "Jose Manuel Calderon", 37);
        laia = new StandardClient("4", "Barcelona", "Laia Palau", 39);
        javier = new VipClient("5", "Segovia", "Javier Guerra", 35);
        ana = new VipClient("6", "Riveira", "Ana Peleteiro", 22);

        dp.addClient(rafa);
        dp.addClient(carolina);
        dp.addClient(jose);
        dp.addClient(laia);
        dp.addClient(javier);
        dp.addClient(ana);
    }

    private void addFavs() {
        rafa.addProduct("rafanint", 1);
        rafa.addProduct("rafaxbox", 2);
        rafa.addProduct("rafalamp", 7);
        rafa.addProduct("rafabarbe", 8);
        rafa.addProduct("rafatowel", 9);
        rafa.addProduct("rafamilk", 20);
        carolina.addProduct("caronint", 1);
        carolina.addProduct("carolamp", 7);
        carolina.addProduct("carobarbe", 8);
        carolina.addProduct("caromilk", 20);
        jose.addProduct("josenint", 1);
        jose.addProduct("josebarbe", 8);
        laia.addProduct("laianint", 1);
        javier.addProduct("javibarbe", 8);
        ana.addProduct("anaxbox", 2);
        ana.addProduct("anabarbe", 8);
        ana.addProduct("anatowel", 9);
        ana.addProduct("anabread", 18);
        ana.addProduct("anacoffee", 19);
        ana.addProduct("anamilk", 20);


    }

    private void addProducts() {
        product1 = new Entertainment(1, "Nintendo Switch", 4, 2, 300f);
        product2 = new Entertainment(2, "Xbox One", 5, 2, 220f);
        product3 = new Entertainment(3, "Samsung Galaxy Tablet", 50, 8, 180f);
        product4 = new Entertainment(4, "Wireless Headphone", 60, 5, 32f);
        product5 = new Entertainment(5, "Kindle", 40, 10, 80f);
        product6 = new Entertainment(6, "Amazon Echo", 10, 3, 100f);
        product7 = new Home(7, "Lamp", 5, 2, 17f, "livingroom");
        product8 = new Home(8, "Barbecue", 2, 1, 80f, "garden");
        product9 = new Home(9, "Towel", 10, 5, 9f, "bathroom");
        product10 = new Home(10, "Coffe Machine", 15, 10, 55f, "kitchen");
        product11 = new Home(11, "Nordic Filling", 2, 1, 60f, "bethroom");
        product12 = new Home(12, "Table", 5, 1, 100f, "livingroom");
        product13 = new Home(13, "Curtains", 2, 1, 40.5f, "bedroom");
        product14 = new Home(14, "Carpet", 3, 2, 80f, "livingroom");
        product15 = new Food(15, "Soft drink", 50, 30, 1.5f, "January");
        product16 = new Food(16, "Cookies", 100, 50, 2f, "February");
        product17 = new Food(17, "Rice", 80, 40, 1.5f, "December");
        product18 = new Food(18, "Bread", 50, 25, 1.2f, "December");
        product19 = new Food(19, "Coffee", 150, 100, 1f, "November");
        product20 = new Food(20, "Milk", 100, 50, 0.6f, "December");

        dp.addProduct(product1);
        dp.addProduct(product2);
        dp.addProduct(product3);
        dp.addProduct(product4);
        dp.addProduct(product5);
        dp.addProduct(product6);
        dp.addProduct(product7);
        dp.addProduct(product8);
        dp.addProduct(product9);
        dp.addProduct(product10);
        dp.addProduct(product11);
        dp.addProduct(product12);
        dp.addProduct(product13);
        dp.addProduct(product14);
        dp.addProduct(product15);
        dp.addProduct(product16);
        dp.addProduct(product17);
        dp.addProduct(product18);
        dp.addProduct(product19);
        dp.addProduct(product20);
    }

    @Before
    public void setUp() {
        dp = StockManager.constructor("DProducts");
        addClients();
        addProducts();
        addFavs();

    }

}
