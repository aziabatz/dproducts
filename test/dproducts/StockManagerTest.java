/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts;

import dproducts.clients.VipClient;
import dproducts.products.Entertainment;
import dproducts.products.Product;
import dproducts.products.features.Likeable;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class StockManagerTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <StockManager Class> Tests......");
    }

    @Before
    public void setUp() {
        super.setUp();

    }

    @Test
    public void constructor() {
        StockManager stockM = StockManager.constructor(null);
        assertNotNull(stockM);
        StockManager newSM = StockManager.constructor("New Stock Mag");
        assertSame(stockM, newSM);
    }


    @Test
    public void addProduct() {
        Product prod = new Entertainment(98, "PolyStation", 80, 2, 3.3f);
        assertNull(dp.findProduct(98));
        dp.addProduct(prod);
        assertEquals(prod, dp.findProduct(98));
        dp.addProduct(product1);
        assertEquals(1, Collections.frequency(dp.getStock(), product1));

    }

    @Test
    public void addClient() {
        int clients = dp.getClients().size();

        dp.addClient(ana);
        assertEquals(clients, dp.getClients().size());

        dp.addClient(new VipClient("213", "Nombre", "Mi nombre", 333));
        assertEquals(clients + 1, dp.getClients().size());
    }

    @Test
    public void findProduct() {
        assertNull(dp.findProduct(390009));
        assertNotNull(dp.findProduct(1));
        assertEquals(product2, dp.findProduct(2));
    }

    @Test
    public void numberInStock() {
        assertEquals(0, dp.numberInStock(6543));
        assertEquals(product1.getQuantity(), dp.numberInStock(1));
    }

    @Test
    public void removeProduct() {
        assertNull(dp.removeProduct(new Entertainment(231, "name", 223, 1222, 0f)));
        int size = dp.getStock().size();
        assertNotNull(dp.removeProduct(product1));
        assertEquals(size - 1, dp.getStock().size());
    }

    @Test
    public void getMostCommented() {
        assertEquals("(mostCommentedProduct:<NO PRODUCT WAS COMMENTED>)", dp.getMostCommented());

    }

    @Test
    public void getMostSoldProduct() {
        dp.getSold().clear();
        assertEquals("(mostSoldProduct:<NO PRODUCT WAS SOLD>)", dp.getMostSoldProduct());
        for (int i = 0; i < 30; i++)
            dp.getSold().add(product7);
        assertEquals("(mostSoldProduct:<" + product7.toString() + " " + Collections.frequency(dp.getSold(), product7) + " " + ((Likeable) product7).getLikes() + ">)", dp.getMostSoldProduct());
    }

    @Test
    public void manageDelivery() {
        Product[] products = {
                product1,
                product2,
                product3,
                product9,
        };

        dp.manageDelivery(Arrays.asList(products), 3);
        assertEquals(12, dp.getSold().size());


    }
}
