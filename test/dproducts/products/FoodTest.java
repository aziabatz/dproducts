/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;

import dproducts.TestsRules;
import dproducts.products.features.Likeable;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FoodTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <Food Class> Tests......");
    }


    @Test
    public void testToString() {

        assertEquals("18 Bread 50 25 1.08 DECEMBER", product18.toString());
        assertNotEquals("Milk April ", product20.toString());
    }

    @Test
    public void discount() {
        assertNotEquals("5%", product18.discount());
        assertEquals("10%", product20.discount());
    }

    @Test
    public void getExpirationMonth() {
        assertEquals("DECEMBER", ((Food) product20).getExpirationMonth().toString());
        assertNotEquals("December", ((Food) product20).getExpirationMonth());
        assertNotEquals("November", ((Food) product18).getExpirationMonth());
    }




    @Test
    public void like() {
        ((Likeable)product18).like();
        assertEquals(1,((Likeable) product18).getLikes());
        ((Likeable)product20).like();
        assertNotEquals( 0,((Likeable) product20).getLikes());
    }

    @Test
    public void dislike() {
        ((Likeable)product18).dislike();
        assertEquals(1, ((Likeable) product18).getLikes());
        ((Likeable) product18).dislike();
        assertEquals(2, ((Likeable) product18).getLikes());
        ((Likeable)product20).dislike();
        assertNotEquals(2, ((Likeable) product20).getLikes());

    }

}