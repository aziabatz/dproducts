/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;


import dproducts.TestsRules;
import dproducts.comments.Comment;
import dproducts.exceptions.AlreadyCommentedProductException;
import dproducts.products.features.Commentable;
import dproducts.products.features.Likeable;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class HomeTest extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <Home Class> Tests......");
    }


    @Test
    public void like() {

        ((Likeable)product8).like();
        assertEquals(1,((Likeable) product8).getLikes());
        ((Likeable)product12).like();
        assertNotEquals( 0,((Likeable) product12).getLikes());

        ((Likeable)product8).dislike();
        assertEquals(2,((Likeable) product8).getLikes());
        ((Likeable)product12).dislike();
        assertNotEquals( 0,((Likeable) product12).getLikes());
    }

    @Test
    public void getLikes() {

        ((Likeable)product8).like();
        assertEquals(1, ((Likeable) product8).getLikes());
        ((Likeable)product12).like();
        assertNotEquals(2, ((Likeable) product12).getLikes());
    }

    @Test
    public void dislike() {

        ((Likeable)product8).dislike();
        assertEquals(1, ((Likeable) product8).getLikes());
        ((Likeable)product12).dislike();
        assertNotEquals(2, ((Likeable) product12).getLikes());
    }

    @Test
    public void getPrice() {

        assertEquals(95f, product12.getPrice(), 0.002f);
        assertNotEquals(80f, product8.getPrice());
    }

    @Test
    public void comment() {
        try {
            assertTrue(((Commentable) product8).comment(new Comment("com", rafa, 2, product8)));
            assertEquals(1, product8.getCommentList().size());
            ((Commentable) product8).comment(new Comment("com", rafa, 2, product8));
            ((Commentable) product8).comment(new Comment("com2", rafa, 4, product8));
            fail("Expected AlreadyCommentedProductException");
        } catch (AlreadyCommentedProductException e) {
            assertThat(e.getMessage(), is("You have already commented this product"));

        } finally {
            assertEquals(1, product8.getCommentList().size());
        }
    }

    @Test
    public void discount() {

        assertEquals("5%", product8.discount());
        assertNotEquals("15%", product12.discount());

    }

    @Test
    public void testToString() {

        assertEquals("8 Barbecue 2 1 76.0 garden", product8.toString());
        assertNotEquals("Table bathroom", product12.toString());
    }
}