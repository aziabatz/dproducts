/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;

import dproducts.TestsRules;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ProductTest  extends TestsRules {

    @BeforeClass
    public static void init() {
        System.out.println("Performing <Comment Class> Tests......");
    }

    @Test
    public void getID() {

        assertEquals(1, product1.getID());
        assertNotEquals(3, product2.getID());

    }

    @Test
    public void getName() {

        assertEquals("Kindle", product5.getName());
        assertNotEquals("Xbox 360", product2.getName());
    }

    @Test
    public void getQuantity() {

        assertEquals(5, product2.getQuantity());
        assertNotEquals(3, product1.getQuantity());
    }

    @Test
    public void getMinimumQuantity() {
        assertEquals(2, product2.getMinimumQuantity());
        assertNotEquals(3, product1.getMinimumQuantity());

    }

    @Test
    public void getPrice() {

        assertNotEquals(350f, product1.getPrice());
        assertEquals(264f, product2.getPrice(), 0.002f);

    }

    @Test
    public void testToString() {
        assertEquals("1 Nintendo Switch 4 2 360.0", product1.toString());
        assertNotEquals("3 Xbox 360 5 2 250f", product2.toString());
    }

    @Test
    public void increaseQuantity() {
        product1.increaseQuantity(1);
        assertEquals(5, product1.getQuantity());
        product2.increaseQuantity(-1);
        assertNotEquals(5 - 1, product2.getQuantity());
    }

    @Test
    public void sellOne() {
        product1.sellOne();
        assertEquals(3, product1.getQuantity());
        product2.sellOne();
        assertNotEquals(5, product2.getQuantity());
    }

    @Test
    public void compareTo() {

        assertEquals(-1,  product1.compareTo(product2));
        assertEquals(1,  product2.compareTo(product1));
        assertEquals(0, product12.compareTo(product12));
    }

    @Test
    public void sellQuantity() {
        product2.sellQuantity(2);
        assertEquals(3, product2.getQuantity());
        product1.sellQuantity(5);
        assertNotEquals(0, product2.getQuantity());
//        fail("TRYING TO SELL WITH NO ENOUGH STOCK");
    }

    @Test
    public void testHashCode() {
        assertNotEquals(2, product1.hashCode());
        assertEquals(2, product2.hashCode());
    }
}