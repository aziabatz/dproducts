/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.visual;


import dproducts.visual.buttons.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class VisualDProducts extends JFrame {
    private final Dimension dimensions = new Dimension(500, 300);
    private GridLayout layoutManager;

    public VisualDProducts() {
        layoutManager = new GridLayout();
        this.setTitle("DP Products");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(layoutManager);
        //this.setMaximumSize(dimensions);
        //this.setMinimumSize(dimensions);

        if (System.getProperty("os.name").contains("Mac")) {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            try {
                Image img = ImageIO.read(getClass().getResource("/appIcon.png"));
                //com.apple.eawt.Application.getApplication().setDockIconImage(img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.setJMenuBar(new VisualDPMenuBar());

        Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/appIcon.png"));
        ImageIcon icon = new ImageIcon();
        setIconImage(icon.getImage());

        add(new ClientsButton());
        add(new ExistingProductsButton());
        add(new SoldProductsButton());
        add(new CommentButton());
        add(new InitButton(this));

        this.setResizable(false);


        this.pack();
        Dimension d = this.getSize();
        //this.setSize(800,(int)d.getHeight());
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
