/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.visual.frames;

import dproducts.StockManager;
import dproducts.clients.Client;
import dproducts.clients.VipClient;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientsFrame extends TableFrame {
    private JFrame frame = this;
    private JTable clientsTable;
    private String[] columns = {"ID", "Location", "Name", "Age", "Type"};
    private JButton moreOrdersClient = new JButton("Clients with more orders");
    private JButton spentMoreClient = new JButton("Who spent more money");

    public ClientsFrame() {
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GridBagLayout layout = new GridBagLayout();
        //setSize(300, 300);
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        clientsTable = new JTable(tableModel) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (Client c :
                StockManager.constructor(null).getClients()) {
            String type = "standard";
            if (c instanceof VipClient)
                type = "VIP";
            Object[] row = {c.getId(), c.getLocation(), c.getName(), c.getAge(), type};
            tableModel.addRow(row);
        }
        //tableModel.addRow(StockManager.constructor().getClients().toArray());
        this.setLayout(layout);
        GridBagConstraints gbd = new GridBagConstraints();
        gbd.gridy = 0;
        gbd.gridx = 0;
        gbd.fill = GridBagConstraints.HORIZONTAL;
        gbd.weighty = 1.0;
        gbd.anchor = GridBagConstraints.NORTHEAST;
        JScrollPane scrollPane = new JScrollPane(clientsTable);
        this.getContentPane().add(scrollPane, gbd);

        //buttons
        initButtonActions();
        gbd.gridy = 1;
        gbd.gridx = 0;
        gbd.anchor = GridBagConstraints.SOUTHWEST;
        this.getContentPane().add(moreOrdersClient, gbd);
        gbd.anchor = GridBagConstraints.SOUTHEAST;
        gbd.gridy = 2;
        this.getContentPane().add(spentMoreClient, gbd);


        pack();
        setLocationRelativeTo(null);

        this.setVisible(true);
    }

    private void initButtonActions() {
        moreOrdersClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //More order dialog
                String orders = StockManager.constructor(null).getClientOrders();
                JOptionPane.showMessageDialog(frame,
                        "The client who ordered more was:\n" + orders,
                        moreOrdersClient.getText(),
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });

        spentMoreClient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //spent more dialog
                String spent = StockManager.constructor(null).getClientSpent();
                JOptionPane.showMessageDialog(frame,
                        "The client who spent more money was:\n" + spent,
                        spentMoreClient.getText(),
                        JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }


}
