/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.visual.frames;

import dproducts.StockManager;
import dproducts.products.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class SoldFrame extends TableFrame {

    private JFrame frame = this;
    private JTable productsTable;
    //(product:<id ​ name stockQuantity minQuantity price [homePart] [expirationMonth]>​ )
    private String[] columns = {"ID", "Name", "Quantity", "Min Quant.", "price"};

    public SoldFrame() {
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GridBagLayout layout = new GridBagLayout();
        //setSize(300, 300);
        DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        productsTable = new JTable(tableModel) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (Product c :
                StockManager.constructor(null).getSold()) {
            Object[] row = {c.getID(), c.getName(), c.getQuantity(), c.getMinimumQuantity(), c.getPrice()};
            tableModel.addRow(row);
        }
        //tableModel.addRow(StockManager.constructor().getClients().toArray());
        this.setLayout(layout);
        GridBagConstraints gbd = new GridBagConstraints();
        gbd.gridy = 0;
        gbd.gridx = 0;
        gbd.fill = GridBagConstraints.HORIZONTAL;
        gbd.weighty = 1.0;
        gbd.anchor = GridBagConstraints.NORTHEAST;
        JScrollPane scrollPane = new JScrollPane(productsTable);
        this.getContentPane().add(scrollPane, gbd);

        //buttons


        pack();
        setLocationRelativeTo(null);

        this.setVisible(true);
    }
}
