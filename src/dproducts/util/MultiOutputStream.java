/*
 * Copyright (c) 2018, AHMED ZIABAT
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of AHMED ZIABAT nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL AHMED ZIABAT BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.util;

import java.io.IOException;
import java.io.OutputStream;

public class MultiOutputStream extends OutputStream {
    /**
     * Flujos de salida
     */
    OutputStream[] outputStreams;

    /**
     * Constructor parametrizado
     * @param outputStreams flujos de salida
     */
    public MultiOutputStream(OutputStream... outputStreams) {
        this.outputStreams = outputStreams;
    }

    /**
     * {@link OutputStream#write(int)}
     */
    @Override
    public void write(int b) throws IOException {
        for (OutputStream out : outputStreams)
            out.write(b);
    }

    /**
     * {@link OutputStream#write(byte[])}
     */
    @Override
    public void write(byte[] b) throws IOException {
        for (OutputStream out : outputStreams)
            out.write(b);
    }

    /**
     * {@link OutputStream#write(byte[], int, int)}
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        for (OutputStream out : outputStreams)
            out.write(b, off, len);
    }

    /**
     * {@link OutputStream#flush()}
     */
    @Override
    public void flush() throws IOException {
        for (OutputStream out : outputStreams)
            out.flush();
    }

    /**
     * {@link OutputStream#close()}
     */
    @Override
    public void close() throws IOException {
        for (OutputStream out : outputStreams)
            out.close();
    }
}
