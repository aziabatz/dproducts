/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.util;

import dproducts.StockManager;
import dproducts.clients.Client;
import dproducts.clients.StandardClient;
import dproducts.clients.VipClient;
import dproducts.exceptions.InvalidInitFileFormatException;
import dproducts.products.Entertainment;
import dproducts.products.Food;
import dproducts.products.Home;
import dproducts.products.Product;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * FileLoader class
 * It handles input files and load them to the program
 */
public class FileLoader {
    /**
     * Method that loads an input file to the program
     *
     * @param file The file to be loaded
     * @throws IOException                    For files(no-existing files) and permission problems
     * @throws InvalidInitFileFormatException When the file hasn't the expected format
     */
    public static void load(File file) throws IOException, InvalidInitFileFormatException {
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String[] tokens;
        StockManager stockManager = null;
        Product product;
        Client client;
        if (br.readLine().equals("!DPRODUCTS!CONFIGURATION!FILE")) {
            do {
                line = br.readLine();
                if (line != null && !line.startsWith("--")) {
                    tokens = line.split("#");
                    switch (tokens[0]) {
                        case "STOCKMANAGER":
                            StockManager.destroy();
                            stockManager = StockManager.constructor(tokens[1]);
                            break;

                        case "PRODUCTENTERTAINMENT":
                            //#id#name#stock#minimum_in_stock#price#
                            product = new Entertainment(Integer.parseInt(tokens[1]), tokens[2], Integer.parseInt(tokens[3]),
                                    Integer.parseInt(tokens[4]), Float.parseFloat(tokens[5]));
                            stockManager.addProduct(product);
                            break;
                        case "PRODUCTHOME":
                            product = new Home(Integer.parseInt(tokens[1]), tokens[2], Integer.parseInt(tokens[3]),
                                    Integer.parseInt(tokens[4]), Float.parseFloat(tokens[5]), tokens[6]);
                            stockManager.addProduct(product);
                            break;
                        case "PRODUCTFOOD":
                            product = new Food(Integer.parseInt(tokens[1]), tokens[2], Integer.parseInt(tokens[3]),
                                    Integer.parseInt(tokens[4]), Float.parseFloat(tokens[5]), tokens[6]);
                            stockManager.addProduct(product);
                            break;

                        //#id#name#age#city#
                        case "CLIENTSTANDARD":
                            client = new StandardClient(tokens[1], tokens[4], tokens[2], Integer.parseInt(tokens[3]));
                            stockManager.addClient(client);
                            break;
                        case "CLIENTVIP":
                            client = new VipClient(tokens[1], tokens[4], tokens[2], Integer.parseInt(tokens[3]));
                            stockManager.addClient(client);
                            break;

                        case "PRODUCTCLIENT": {
                            for (Client c :
                                    stockManager.getClients()) {
                                if (c.getId().equals(tokens[1])) {
                                    c.addProduct(tokens[3], Integer.parseInt(tokens[2]));
                                }
                            }
                            break;
                        }
                    }
                }
            } while (line != null);
        } else
            throw new InvalidInitFileFormatException();
    }
}
