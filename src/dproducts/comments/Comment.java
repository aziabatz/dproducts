/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.comments;

import dproducts.clients.Client;
import dproducts.products.Product;

/**
 * dproducts.comments.Comment class
 *
 * @author Ahmed Ziabat Ziabat and José Miguel Bermejo González
 * @version 1.0
 */
public class Comment implements Comparable<Comment> {
    /**
     * Text of the comment
     */
    private final String comment;
    /**
     * Who wrote the comment
     */
    private final Client client;
    /**
     * Rating of the product
     */
    private final int rating;

    /**Commented product*/
    private final Product product;

    public static final String[] DEFAULT_COMMENTS =
            {
                    "Bad product",
                    "Not very good product",
                    "Good product",
                    "Very good product",
                    "Excellent product"
            };

    /**
     * Constructor of the object
     *
     * @param comment Text of the comment
     * @param client Client who commented
     * @param rating Rating or score of the product
     * @param product Comment product
     */
    public Comment(String comment, Client client, int rating, Product product) {
        this.comment = comment;
        this.client = client;
        this.rating = rating;
        this.product = product;
    }

    /**
     * @return The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return The client who wrote the comment
     */
    public Client getClient() {
        return client;
    }

    /**
     * @return Rating or mark of the product
     */
    public int getRating() {
        return rating;
    }

    /**
     * @return Reference to the commented product
     */
    public Product getProduct() {
        return product;
    }

    @Override
    public String toString() {
        return client.getName() + " " + comment + " " + rating;
    }

    @Override
    public int compareTo(Comment o) {
        if (this.getRating() > o.getRating())
            return -1;
        else if (this.getRating() < o.getRating())
            return 1;
        else
            return o.getClient().getName().compareTo(this.client.getName());
    }
}
