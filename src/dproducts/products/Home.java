/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;

import dproducts.comments.Comment;
import dproducts.exceptions.AlreadyCommentedProductException;
import dproducts.products.features.Commentable;
import dproducts.products.features.Likeable;

import java.util.PriorityQueue;

/**
 * Home products
 *
 * @author Ahmed Ziabat Ziabat
 */
public class Home extends Product implements Commentable, Likeable {
    /**
     * Available home parts
     */
    private static String[] HOMEPARTS = {"livingroom", "kitchen", "bedroom", "bathroom", "garden"};
    /**
     * Home part of this product
     */
    private final String homePart;
    /**Applied discount of this product*/
    private final float discount = 0.95f;
    /**Total likes and unlikes of this products*/
    int likes;
    /**Likes rating*/
    int likes_rating;

    /**
     * Constructor
     * @param id Product id
     * @param name Product name
     * @param quantity Product quantity
     * @param minimumQuantity Product minimum quantity
     * @param price Product price
     * @param part Homepart
     */
    public Home(int id, String name, int quantity, int minimumQuantity, float price, int part) {
        this.id = id;
        this.name = name;
        this.quantity = Math.abs(quantity);
        this.minimumQuantity = Math.abs(minimumQuantity);
        this.price = price;
        this.homePart = HOMEPARTS[part];
        likes=0;
        likes_rating = 0;
        commentList = new PriorityQueue<>();
    }

    /**
     * Constructor
     *
     * @param id              Product id
     * @param name            Product name
     * @param quantity        Product quantity
     * @param minimumQuantity Product minimum quantity
     * @param price           Product price
     * @param homePart        Homepart
     */
    public Home(int id, String name, int quantity, int minimumQuantity, float price, String homePart) {
        this.id = id;
        this.name = name;
        this.quantity = Math.abs(quantity);
        this.minimumQuantity = Math.abs(minimumQuantity);
        this.price = price;
        this.homePart = homePart;
        likes = 0;
        likes_rating = 0;
        commentList = new PriorityQueue<>();
    }

    @Override
    public void like() {
        likes++;
        likes_rating++;
    }

    @Override
    public int getLikes() {
        return likes;
    }

    @Override
    public void dislike() {
        likes++;
        likes_rating--;
    }

    @Override
    public float getPrice() {
        return discount * super.getPrice();
    }

    @Override
    public boolean comment(Comment comment) throws AlreadyCommentedProductException {
        boolean c = false;
        for (Comment cm :
                commentList) {
            if (comment.getClient() == cm.getClient())
                c = true;
        }

        if (c) {
            throw new AlreadyCommentedProductException();
            //return false;
        } else {
            this.getCommentList().add(comment);
            //return true;
        }
        return !c;
    }

    @Override
    public String discount() { return "5%"; }

    @Override
    public String toString() {
        return super.toString() + " " + homePart;
    }
}
