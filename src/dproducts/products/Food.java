/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;

import dproducts.products.features.Likeable;

import java.time.Month;

/**
 * Food product
 *
 * @author Ahmed Ziabat Ziabat
 * @version 1.0
 */
public class Food extends Product implements Likeable {
    /**
     * Discount of the product
     */
    private final float discount = 0.9f;
    /**
     * Total likes and unlikes of the products
     */
    int likes;
    /**
     * Expiration month of the product
     */
    private Month expirationMonth;

    /**
     * Constructor
     * @param id Product id
     * @param name Product name
     * @param quantity Product quantity
     * @param minimumQuantity Product minimum quantity
     * @param price Product price
     * @param expiration Expiration month
     */
    public Food(int id, String name, int quantity, int minimumQuantity, float price, int expiration) {
        this.id = id;
        this.name = name;
        this.quantity = Math.abs(quantity);
        this.minimumQuantity = Math.abs(minimumQuantity);
        this.price = price;
        this.likes=0;
        expirationMonth = Month.of(expiration);

    }

    /**
     * Constructor
     *
     * @param id              Product id
     * @param name            Product name
     * @param quantity        Product quantity
     * @param minimumQuantity Product minimum quantity
     * @param price           Product price
     * @param expiration      Expiration month
     */
    public Food(int id, String name, int quantity, int minimumQuantity, float price, String expiration) {
        this.id = id;
        this.name = name;
        this.quantity = Math.abs(quantity);
        this.minimumQuantity = Math.abs(minimumQuantity);
        this.price = price;
        this.likes = 0;
        expirationMonth = Month.valueOf(expiration.toUpperCase());
    }

    @Override
    public String toString() {
        return super.toString() + " " + expirationMonth.toString();
    }

    @Override
    public String discount() {
        return "10%";
    }

    /**
     * @return The expiration month of the product
     */
    public Month getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * @return The discount of the product
     */
    public float getDiscount() {
        return discount;
    }

    /**
     * @return Price of the product
     */
    public float getPrice() {
        return discount * super.getPrice();
    }

    @Override
    public void like() {
        likes++;
    }

    @Override
    public void dislike() {
        likes++;
    }

    @Override
    public int getLikes() {
        return likes;
    }
}
