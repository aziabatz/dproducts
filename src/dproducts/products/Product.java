/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.products;

import dproducts.comments.Comment;

import java.util.Queue;

/**
 * Model some details of a product sold by a company.
 *
 * @author David J. Barnes and Michael Kölling.
 * @version 2011.07.31
 */
public abstract class Product implements Comparable<Product>
{
    /**
     * An identifying number for this product.
     */
    protected int id;
    /**
     * The name of this product.
     */
    protected String name;
    /**
     * The quantity of this product in stock.
     */
    protected int quantity;
    /**
     * The minimum quantity the stock manager must have
     */
    protected int minimumQuantity;
    /**
     * A list with all the received comments of this products
     */
    protected Queue<Comment> commentList;

    /**
     * The price of the amount of a product
     */
    protected float price;

    /**
     * Default constructor
     */
    public Product(){}

    /**
     * @return The product's id.
     */
    public int getID() {
        return id;
    }

    /**
     * @return The product's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The quantity in stock.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return The minimum quantity
     */
    public int getMinimumQuantity() {
        return minimumQuantity;
    }

    /**
     * @return The price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @return The list with all its comments
     */
    public Queue<Comment> getCommentList() {
        return commentList;
    }

    /**
     * @return The id, name and quantity in stock.
     */
    public String toString() {
        //(product:<id ​ name stockQuantity minQuantity price [homePart] [expirationMonth]>​ )
        return id + " " + name + " " + quantity + " " + minimumQuantity + " " + getPrice();
    }

    /**
     * @return A string with the percentage of applied discount
     */
    public abstract String discount();

    /**
     * Restock with the given amount of this product.
     * The current quantity is incremented by the given amount.
     * @param amount The number of new items added to the stock.
     *               This must be greater than zero.
     */
    public void increaseQuantity(int amount) {
        if(amount > 0) {
            quantity += amount;
            if (quantity < minimumQuantity)
                quantity += (minimumQuantity - quantity);
        } else {
            System.err.println("Attempt to restock " +
                    name +
                    " with a non-positive amount: " +
                    amount);
        }
    }

    /**
     * Sell one of these products.
     * An error is reported if there appears to be no stock.
     */
    public void sellOne() {
        if(quantity > 0) {
            quantity--;
        } else {
            System.out.println(
                    "Attempt to sell an out of stock item: " + name);
        }
    }

    @Override
    public int compareTo(Product o) {
        float price = o.getPrice();
        if(getPrice()>price)
            return -1;
        else
            if(getPrice()<price)
                return 1;
            else
                return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return id == ((Product) obj).getID();
    }

    /**
     * Sells specified amount of this product
     *
     * @param amount How many products must be sold
     */
    public void sellQuantity(int amount) {
        if (quantity < amount) {
            System.out.println("TRYING TO SELL WITH NO ENOUGH STOCK of " + name);
            increaseQuantity(1);
        } else {
            System.out.println("Sold " + amount + " of " + name + " actual quant. " + quantity);
            quantity -= amount;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
