/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;

import java.util.LinkedList;
import java.util.List;

/**
 * VipClient class
 *
 * @author Ahmed Ziabat Ziabat
 * @version 1.0
 */
public class VipClient extends Client {

    /**
     * Constructor
     *
     * @param id       Id
     * @param location Location
     * @param name     Name
     * @param age      Age
     */
    public VipClient(String id, String location, String name, int age) {
        super(id, location, name, age);
    }

    @Override
    public List<Product> getOrder() {
        List<Product> products = new LinkedList<>();
        products.addAll(deliveryList);
        return products;
    }

    @Override
    public float getPrice() {
        float price=0f;
        for (Product p :
                deliveryList) {
            price=price+p.getPrice();
        }
        return price;
    }

    @Override
    public void makeOrder() throws VoidOrderException {
        if (deliveryList.isEmpty()) {
            throw new VoidOrderException();
        } else {
            super.makeOrder();
            numberOfOrders++;
            getStockManager().manageDelivery(getOrder(), 1);
            deliveryList.clear();
        }
    }

    @Override
    public void makeComment(Product product) {
        int rating = 4;
        String comment = "​I really like this product";
        tryToComment(comment, product, rating);
        rateProduct(product, rating);
    }
}
