/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.StockManager;
import dproducts.comments.Comment;
import dproducts.exceptions.AlreadyCommentedProductException;
import dproducts.exceptions.CannotCommentProductException;
import dproducts.exceptions.NotInFavoritesException;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;
import dproducts.products.features.Commentable;
import dproducts.products.features.Likeable;

import java.util.*;

/**
 * dproducts.clients.Client class
 *
 * @author Ahmed Ziabat Ziabat and José Miguel Bermejo González
 * @version 1.2
 */
public abstract class Client {

    /**
     * ID of the client
     */
    protected String id;
    /**
     * Location of the client
     */
    protected String location;
    /**
     * Name of the client
     */
    protected String name;
    /**
     * Age of the client
     */
    protected int age;

    /**
     * Default stock manager from where the client buys
     * */
    protected StockManager stockManager;


    /** Favorite products of the client
     * String is the key, dproducts.products.Product is the value
     * */
    protected Map<String, Product> favoriteProducts;

    /**
     * Comments done by the client
     */
    protected List<Comment> comments;

    /**
     * Order list
     */
    protected PriorityQueue<Product> deliveryList;

    /**
     * Orders done
     */
    protected int numberOfOrders;

    /**
     * Spent money
     */
    private float spentMoney;

    /**
     * Default constructor
     */
    public Client () {}

    /**
     * Object constructor
     * @param id ID of the client
     * @param location Location of the client
     * @param name Name of the client
     * @param age Age of the client
     */
    public Client(String id, String location, String name, int age) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.age = age;
        stockManager = null;
        favoriteProducts = new HashMap<>();
        comments = new ArrayList<>();
        deliveryList = new PriorityQueue<>();
        numberOfOrders = 0;
        spentMoney = 0f;
    }

    /**
     * Object constructor
     * @param id ID of the client
     * @param location Location of the client
     * @param name Name of the client
     * @param age Age of the client
     * @param stockManager Stock manager which the client belongs to
     *
     */
    public Client(String id, String location, String name, int age, StockManager stockManager) {
        this.id = id;
        this.location = location;
        this.name = name;
        this.age = age;
        this.stockManager = stockManager;
        favoriteProducts = new HashMap<>();
        comments = new ArrayList<>();
    }

    @Override
    public String toString() {
        //(client:<id name age city>)
        return id + " " + name + " " + age + " " + location;
    }

    /**
     * @return ID of the client
     */
    public String getId() {
        return id;
    }

    /**
     * @param id New client ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return dproducts.clients.Client location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location New location of the client
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return dproducts.clients.Client's name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name New name of the client
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Age of the client
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age New age of the client
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return dproducts.StockManager from which the client buys
     */
    public StockManager getStockManager() {
        return stockManager;
    }

    /**
     * @param stockManager New dproducts.StockManager from which the client will buy
     */
    public void setStockManager(StockManager stockManager) {
        this.stockManager = stockManager;
    }

    /**
     * @return Favorite products
     */
    public Map<String,Product> getFavoriteProducts() {
        return favoriteProducts;
    }

    /**
     * @param favoriteProducts New Favorite products
     */
    public void setFavoriteProducts(Map<String,Product> favoriteProducts) {
        this.favoriteProducts = favoriteProducts;
    }

    /**
     * Method to add products to favorites
     *
     * @param name How the products must be named by the client
     * @param id   ID of the product
     */
    public void addProduct(String name, int id){
        Product product = stockManager.findProduct(id);
        if(product==null){
            //System.out.println(ANSI_RED + "No available product with specified ID:" + id + '\t' + "name:" + name + ANSI_RESET);
        } else{
            //System.out.println(ANSI_GREEN + "Producto añadido a favoritos: " + name + ANSI_RESET);
            favoriteProducts.putIfAbsent(name, product);
        }
    }

    /**
     * Search for a product in the favorite products of this client
     *
     * @param name Name of the product(key)
     * @return The product, if found
     */
    public Product findProduct(String name) {
        Product product;
        product = favoriteProducts.get(name);
        if (product == null)
            System.err.println("No such product\t Name: " + name);
        return product;
    }

    /**
     * Searches for the product in favorites
     *
     * @param product Product to search
     * @return True if found
     */
    public boolean findProduct(Product product) {
        boolean found;
        found = favoriteProducts.containsValue(product);
        if (!found)
            System.err.println("No such product " + product.toString());
        return found;
    }

    /**
     * Removes a product from the favorites list
     *
     * @param name How the client names the product
     */
    public void removeProduct(String name){
        Product product = favoriteProducts.get(name);
        if (product != null) {
            favoriteProducts.remove(name, product);
        }
    }

    /**
     * Deliveries a specified amount of a favorite product
     *
     * @param name   Name of the product
     * @param amount Amount
     */
    public void deliveryProduct(String name, int amount) {
        Product product = favoriteProducts.get(name);
        if (product != null) {
            if (amount == 1)
                product.sellOne();
            else
                product.sellQuantity(amount);
        } else {
            System.err.println("No such product in favorite list");
        }
    }

    /**
     * Delivers a product of each.<br>
     * If a product is out of stock, then it is not delivered.(the product)
     */
    public void deliveryEachOneProduct() {
        List<Product> fav = new ArrayList<>(favoriteProducts.values());
        for (Product p : fav) {
            p.sellOne();
        }
    }

    /**
     * It adds a product to the actual order
     * @param name Name of the product
     */
    public void addToOrder(String name) {
        Product product = findProduct(name);
        if (product != null) {
            deliveryList.offer(product);
            //System.out.println(ANSI_YELLOW + "ADDED TO ORDER: " + product.getName() + ANSI_RESET);
        } else
            try {
                throw new NotInFavoritesException();
            } catch (NotInFavoritesException nife) {
                System.err.println("There is no such " + name + " in your favorites list");
            }
    }

    /**
     * @return Returns how much orders was done byt he client
     */
    public int getNumberOfOrders() {
        return numberOfOrders;
    }

    /**
     * @return Returns the total amount of money spent by the client
     */
    public float getSpentMoney() {
        return spentMoney;
    }

    /**
     * @return List of products that will be ordered
     */
    public abstract List<Product> getOrder();

    /**
     * @return Price of the order
     */
    public abstract float getPrice();

    /**
     * Performs the order communicating to the StockManager
     * @throws VoidOrderException if the order has no products
     */
    public void makeOrder() throws VoidOrderException {
        System.out.println("(client:<" + toString() + ">)");
        float orderPrice = getPrice();
        spentMoney = spentMoney + orderPrice;
    }

    /**
     * Rates the product
     * @param product Product rated
     * @param rating Rating or score
     */
    protected void rateProduct(Product product, int rating) {
        if (product instanceof Likeable) {
            if (rating >= 4)
                ((Likeable) product).like();
            if (rating <= 2)
                ((Likeable) product).dislike();
        }
    }

    /**
     * @return Return a list with all the comments of the client
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @return Return a queue(sort by price) of the client's delivery list
     */
    public PriorityQueue<Product> getDeliveryList() {
        return deliveryList;
    }

    /**
     * Adds a comment to a favorite product
     *
     * @param comment The text of the comment
     * @param product The commented product
     * @param rating  Rating of the product by the client
     * @throws AlreadyCommentedProductException If we try to comment a commented product by the this client
     * @throws CannotCommentProductException    If the client tries to comment a food product
     * @throws NotInFavoritesException If the product is not in client's favourite list
     */
    public void addComment(String comment, Product product, int rating) throws AlreadyCommentedProductException, CannotCommentProductException, NotInFavoritesException {
        if (favoriteProducts.containsValue(product)) {
            if (product instanceof Commentable) {
                Commentable commentableProduct = (Commentable) product;
                Comment cmt = new Comment(comment, this, rating, product);
                if (commentableProduct.comment(cmt)) {
                    comments.add(cmt);
                }
            } else {
                throw new CannotCommentProductException();
            }
        } else {
            throw new NotInFavoritesException();
        }
    }

    /**
     * Comments a product
     * @param product Commented product
     */
    public abstract void makeComment(Product product);

    /**
     * Common method to comment handling any possible exception
     *
     * @param comment The text of the comment
     * @param product The commented product
     * @param rating  The rating
     */
    protected void tryToComment(String comment, Product product, int rating) {
        try {
            addComment(comment, product, rating);
        } catch (AlreadyCommentedProductException e) {
            System.err.println("You have already commented this product");
        } catch (CannotCommentProductException e) {
            System.err.println("You can't comment food");
        } catch (NotInFavoritesException e) {
            System.err.println("Not in favs");
        }
    }
}
