/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts.clients;

import dproducts.comments.Comment;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;

import java.util.LinkedList;
import java.util.List;

/**
 * StandardClient class
 *
 * @author Ahmed Ziabat Ziabat
 * @version 1.0
 */
public class StandardClient extends Client {

    /**
     * Constructor
     *
     * @param id       Id
     * @param location Location
     * @param name     Name
     * @param age      Age
     */
    public StandardClient(String id, String location, String name, int age) {
        super(id, location, name, age);
    }

    @Override
    public List<Product> getOrder() {
        List<Product> products = new LinkedList<>();
        products.addAll(deliveryList);
        if (products.size() >= 2)
            return products.subList(0, 2);
        else
            return products.subList(0, 1);
    }

    @Override
    public float getPrice() {
        List<Product> products = getOrder();
        float price = 0;
        for (Product p :
                products) {
            price = price + (50 * p.getPrice());
        }
        return price;
    }

    @Override
    public void makeOrder() throws VoidOrderException {
        if (deliveryList.isEmpty()) {
            throw new VoidOrderException();
        } else {
            super.makeOrder();
            getStockManager().manageDelivery(getOrder().subList(0, 1), 50);
            numberOfOrders++;
            if (getOrder().size() > 1) {
                getStockManager().manageDelivery(getOrder().subList(1, 2), 50);
                numberOfOrders++;
            }
            deliveryList.clear();
        }
    }

    @Override
    public void makeComment(Product product) {
        //addComment(String comment, Product product, int rating)
        int rating = (product.getName().length() % 5) + 1;
        if (rating > 5)
            rating = 5;

        String comment = Comment.DEFAULT_COMMENTS[(rating - 1)];
        tryToComment(comment, product, rating);
        rateProduct(product, rating);
    }
}
