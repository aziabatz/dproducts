/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts;

import dproducts.exceptions.InvalidInitFileFormatException;
import dproducts.util.FileLoader;
import dproducts.util.MultiOutputStream;
import dproducts.visual.VisualDProducts;

import java.io.*;

/**
 * Main class of our project
 */
public class DProducts {
    /**
     * Initial point of the application
     * Called from this class or from the GUI
     * @param file The file to be read
     * @throws InvalidInitFileFormatException If the file hasn't a valid format
     */
    public static void init(File file) throws InvalidInitFileFormatException {
        try {
            FileOutputStream log = new FileOutputStream("registro.log");
            MultiOutputStream multiOutputStream = new MultiOutputStream(log, System.out);
            PrintStream out = new PrintStream(multiOutputStream);
            System.setOut(out);
            FileLoader.load(file);
            StockDemo stockDemo = new StockDemo();
            stockDemo.demo();
        } catch (FileNotFoundException fne) {
            fne.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Main method
     *
     * @param args External arguments
     */
    public static void main(String[] args) {
        VisualDProducts vp;
        if (args.length > 0) {
            try {
                init(new File(args[0]));
            } catch (InvalidInitFileFormatException e) {
                e.printStackTrace();
            }
        } else {
            System.err.println("You must pass at least an argument\nUsage: dproducts [filename]");
            System.out.println("Using GUI interface");
            vp = new VisualDProducts();
        }


    }
}
