/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts;

import dproducts.clients.Client;
import dproducts.comments.Comment;
import dproducts.exceptions.OutOfStockException;
import dproducts.products.Product;
import dproducts.products.features.Likeable;

import java.util.*;

/**
 * Manage the stock in a business.
 * The stock is described by zero or more Products.
 *
 * @author Ahmed Ziabat Ziabat and José Miguel Bermejo González
 * @version 1.3
 */
public class StockManager
{
    /**
     * Name of the stock manager
     */
    private String name;
    /**
     * A list of the products
     */
    private ArrayList<Product> stock;

    /**
     * List with the sold products
     * We use arraylist because it allows duplicates
     */
    private ArrayList<Product> sold;

    /**
     * Single instance of this class
     */
    private static StockManager stockManager;

    /**
     * Clients of this stock manager
     */
    private List<Client> clients;

    /**
     * Products that must be replenished after an order
     */
    private Set<Product> replenishedSet;

    /**
     * StockManager Constructor
     * @param name Name of the stock manager
     */
    private StockManager(String name) {
        stock = new ArrayList<Product>();
        clients = new LinkedList<>();
        sold = new ArrayList<>();
        replenishedSet = new HashSet<>();
        this.name = name;
    }

    /**
     * Single instance provider
     * @param name Name of the constructor
     * @return Instance of this class
     */
    public static StockManager constructor(String name) {
        if(stockManager==null)
            stockManager = new StockManager(name);
        return stockManager;
    }

    /**
     * Destroys the instance
     */
    public static void destroy() {
        stockManager = null;
    }

    /**
     * Add a product to the list.
     * @param item The item to be added.
     */
    public void addProduct(Product item) {
        boolean found = false;
        for (Product product :
                stock) {
            if (product.getID() == item.getID())
                found = true;
        }
        if (!found)
            stock.add(item);


        if (stock.stream().filter(product -> product.getID() == item.getID()).findAny().orElse(null) == null)
            System.out.println("Sí, encontrado");

    }

    /**
     * Adds a client to the stock manager
     * @param client Client to be added
     */
    public void addClient(Client client) {
        if (!clients.contains(client)) {
            clients.add(client);
            client.setStockManager(this);
        }
    }

    /**
     * @return The stock of products
     */
    public ArrayList<Product> getStock() {
        return stock;
    }

    /**
     * @return The clients who are from this stock manager
     */
    public List<Client> getClients() {
        return clients;
    }

    /**
     * Receive a delivery of a particular product.
     * Increase the quantity of the product by the given amount.
     * @param id The ID of the product.
     * @param amount The amount to increase the quantity by.
     */
    private void delivery(int id, int amount) {
        Product productFound = findProduct(id);

        if (productFound != null) {
            productFound.increaseQuantity(amount);
            //System.out.println("Delivered to StockManager [Name] [Quantity] [Delivered]:"+ '\t' + productFound.getName() + '\t' + productFound.getQuantity() + '\t' + amount);
        }
    }

    /**
     * Try to find a product in the stock with the given id.
     * @param id ID of the product
     * @return The identified product, or null if there is none
     *         with a matching ID.
     */
    public Product findProduct(int id) {
        Product found = null;
        for(Product product: stock){
            if(product.getID()==id) {
                found = product;
                break;
            }
        }
        if (found == null)
            System.err.println("No such product\t ID: " + id);
        else {
            //System.out.println(ANSI_BLUE + found.toString() + ANSI_RESET);
        }

        return found;
    }

    /**
     * Locate a product with the given ID, and return how
     * many of this item are in stock. If the ID does not
     * match any product, return zero.
     * @param id The ID of the product.
     * @return The quantity of the given product in stock.
     */
    public int numberInStock(int id) {
        int amount = 0;
        for(Product product: stock){
            if(product.getID()==id) {
                amount = product.getQuantity();
            }
        }
        return amount;
    }

    /**
     * Removes a product from a list if it exists
     *
     * @param p The product to be removed
     * @return It returns a ref to the recently removed product, if no product was found it returns null.
     */
    public Product removeProduct(Product p) {
        Product prod = null;
        int index = stock.indexOf(p);
        if (index != -1) {
            prod = stock.get(index);
            stock.remove(index);
        }
        return prod;
    }

    /**
     * Print details of all the products.
     */
    public void printProductDetails() {
        for (Product p:
                stock) {
            System.out.println(p.toString());
        }
    }

    /**
     * Searches for the most sold product
     * @return A reference to the most sold product
     */
    private Product mostSoldProducts() {
        Product product = null;
        int repeated = 0;
        Set<Product> products = new HashSet<>(sold);
        for (Product p :
                products) {
            int freq = Collections.frequency(products, p);
            if (freq >= repeated) {
                product = p;
                repeated = freq;
            }
        }

        return product;
    }

    /**
     * Searches for the most commented product
     * @return A reference to the most commented product
     */
    private Product mostCommentedProduct() {
        int comments = 0;
        Product product = null;

        for (Product p :
                stock) {
            if (p.getCommentList() != null) {
                int comment = p.getCommentList().size();
                if (comment >= comments) {
                    product = p;
                    comments = comment;
                }
            }
        }
        if (comments > 0)
            return product;
        else
            return null;
    }

    /**
     * Searches for the client who ordered more
     * @return A reference to the client with more orders
     */
    private Client clientWithMoreOrders() {
        Client client = null;
        int orders = 0;
        for (Client c :
                clients) {
            if (c.getNumberOfOrders() > orders) {
                client = c;
                orders = c.getNumberOfOrders();
            }
        }
        return client;
    }

    /**
     * Searches for the client who spent more
     * @return A reference to the client who spent more money
     */
    private Client clientWhoSpentMore() {
        Client client = null;
        float spent = 0f;
        for (Client c :
                clients) {
            if (c.getSpentMoney() > spent) {
                client = c;
                spent = c.getSpentMoney();
            }
        }
        return client;
    }

    /**
     * @return A list with sold products(repeated by how many times was sold
     */
    public ArrayList<Product> getSold() {
        return sold;
    }

    /**
     * It shows the most commented product
     * @return A string with the most commented product
     */
    public String getMostCommented() {
        Product product = mostCommentedProduct();
        int likes = 0;
        if (product instanceof Likeable) {
            likes = ((Likeable) product).getLikes();
        }
        if (product != null)
            return ("(mostCommentedProduct:<" + product.toString() + " " + likes + " " + product.getCommentList().size() + ">)");
        else
            return ("(mostCommentedProduct:<NO PRODUCT WAS COMMENTED>)");
    }

    /**
     * It shows the most sold product
     * @return A string with the most sold product
     */
    public String getMostSoldProduct() {
        Product product = mostSoldProducts();
        int salesNumber;
        int likes = 0;
        if (product instanceof Likeable) {
            likes = ((Likeable) product).getLikes();
        }
        salesNumber = Collections.frequency(sold, product);
        if (product != null)
            return ("(mostSoldProduct:<" + product.toString() + " " + salesNumber + " " + likes + ">)");
        else
            return ("(mostSoldProduct:<NO PRODUCT WAS SOLD>)");
    }

    /**
     * It shows who ordered more
     * @return A string with the client with more orders
     */
    public String getClientOrders() {
        Client c = clientWithMoreOrders();
        if (c != null)
            return ("(clientWithMoreOrders:<" + c.toString() + " " + c.getNumberOfOrders() + ">)");
        else
            return ("(clientWithMoreOrders:<NOBODY HAS MADE ANY ORDER>)");
    }

    /**
     * It shows who spent more
     * @return A string with the client who spent more
     */
    public String getClientSpent() {
        Client c = clientWhoSpentMore();
        if (c != null)
            return ("(clientWhoSpentMore:<" + c.toString() + " " + c.getSpentMoney() + ">)");
        else
            return ("(clientWhoSpentMore:<NOBODY HAS MADE ANY ORDER>)");
    }

    /**
     * It sells a product
     * @param product Product to be sold
     */
    private void sellProduct(Product product) {
        if (product.getQuantity() == 0) {
            try {
                throw new OutOfStockException(product);
            } catch (OutOfStockException e) {
                //System.err.println(ANSI_RED_BACKGROUND+"out of stock"+ANSI_RESET);
                replenishedSet.add(product);
            }
            delivery(product.getID(), product.getMinimumQuantity());
        }
        //System.out.println(ANSI_BLUE + "(soldProduct:<" + product.toString() + ">)" + ANSI_RESET);
        product.sellOne();
        sold.add(product);

    }

    /**
     * Manages the client deliveries
     * @param deliverList A list with the product to be delivered to the client
     * @param amount How much products must be delivered
     */
    public void manageDelivery(List<Product> deliverList, int amount) {
        for (Product product
                : deliverList) {
            System.out.println("(product:<" + product.toString() + " " + product.discount() + " " + amount + ">)");
            Queue<Comment> commentList = product.getCommentList();
            if (commentList != null) {
                for (Comment comment :
                        commentList) {
                    System.out.println("\t(comment:<" + comment.toString() + ">)");
                }
            }
            for (int i = 0; i < amount; i++) {
                sellProduct(product);
            }
        }
        if (replenishedSet.isEmpty())
            System.out.println("(the order is done)");
        else {
            System.out.println("(the order is done and these products need to be replenished)");
            for (Product p :
                    replenishedSet) {
                System.out.println("(product:<" + p.toString() + ">)");
                if (p.getQuantity() < p.getMinimumQuantity())
                    delivery(p.getID(), amount);
            }
            replenishedSet.clear();
        }
    }
}
