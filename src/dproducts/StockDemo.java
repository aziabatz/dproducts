/*
 * Copyright (c) 2018, ObjectsParsers Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package dproducts;

import dproducts.clients.Client;
import dproducts.comments.Comment;
import dproducts.exceptions.VoidOrderException;
import dproducts.products.Product;

import java.util.HashSet;
import java.util.Queue;

/**
 * Demonstrate the dproducts.StockManager and dproducts.products.Product classes.
 * The demonstration becomes properly functional as
 * the dproducts.StockManager class is completed.
 *
 * @author David J. Barnes and Michael Kölling.
 * @version 2011.07.31
 */
public class StockDemo
{
    /**
     * Maximum number of turns allowed
     */
    private final static int MAXTURNS = 9;

    /**
     * The stock manager
     */
    private StockManager sww;

    /**
     * Create a dproducts.StockManager and populate it with a few
     * sample products.
     */
    public StockDemo() {
        sww = StockManager.constructor(null);



    }

    /**
     * Provide a very simple demonstration of how a dproducts.StockManager
     * might be used. Details of one product are shown, the
     * product is restocked, and then the details are shown again.
     */
    public void demo() {
        int turn = 0;
        System.out.println("(turn:" + turn + ")");

        Client client = sww.getClients().get(1);

        try {
            client.makeOrder();
        } catch (VoidOrderException e) {
            System.err.println(e.toString());
        }


        client.addToOrder("caronint");
        try {
            client.makeOrder();
        } catch (VoidOrderException e) {
            e.printStackTrace();
        }

        client.makeComment(client.findProduct("caronint"));

        System.out.println("<end of simulation>");
        //end of simulation
        HashSet<Product> sold = new HashSet<>();
        sold.addAll(sww.getSold());
        //FOREACH PRODUCT SOLD AND COMMENTED
        //TODO MOVE TO SOLDPRODUCTS() IN STOCKMANAGER
        for (Product p : sold) {
            System.out.println("(soldProduct:<" + p.toString() + ">)");
            Queue<Comment> commentList = p.getCommentList();
            if (commentList != null) {
                for (Comment comment :
                        commentList) {
                    System.out.println("\t(comment:<" + comment.toString() + ">)");
                }
            }
        }

        sww.getMostSoldProduct();
        sww.getMostCommented();

        sww.getClientOrders();
        sww.getClientSpent();

    }

    /**
     * Show details of the given product. If found,
     * its name and stock quantity will be shown.
     * @param id The ID of the product to look for.
     */
    public void showDetails(int id) {
        Product product = getProduct(id);
        if(product != null) {
            System.out.println(product.toString());
        }
    }

    /**
     * Sell one of the given item.
     * Show the before and after status of the product.
     * @param id The ID of the product being sold.
     */
    public void sellProduct(int id) {
        Product product = getProduct(id);

        if(product != null) {
            showDetails(id);
            product.sellOne();
            showDetails(id);
        }
    }

    /**
     * Get the product with the given id from the manager.
     * An error message is printed if there is no match.
     * @param id The ID of the product.
     * @return The dproducts.products.Product, or null if no matching one is found.
     */
    public Product getProduct(int id) {
        Product product = sww.findProduct(id);
        if(product == null) {
            System.out.println("dproducts.products.Product with ID: " + id +
                    " is not recognised.");
        }
        return product;
    }

    /**
     * @return The stock manager.
     */
    public StockManager getManager() {
        return sww;
    }
}
