# ---------------DProducts--------------

# Información:

## Descripción del proyecto:
Este proyecto busca imitar el funcionamiento de una empresa que se dedica a la venta de productos, similar a las tiendas online como "Amazon". Las operaciones más importantes son los pedidos de productos por parte de la compañía(StockManager) y de los clientes del mismo, junto con un constante intento por mantener un stock mínimo.
Los clientes por su parte piden productos a un solo StockManager y que a su vez estén en su lista de favoritos, pueden crear comentarios acerca de los productos que tiene entre sus favoritos. 

## Clases:

### Client:
 Es la clase que representa al cliente(con sus correspondientes parámetros) y la asignación del mismo a un "StockManager", 
 sus métodos están dedicados al pedido de los productos a la tienda, guardarlos como favorito o crear y guardar un comentario sobre  un producto. Actualmente es una clase abstracta, no se puede instanciar por sí sola.

#### VipClient:
Una de las subclases de la clase Client, su función es darle características únicas al cliente(VIP) durante la compra y la realización de comentarios, como por ejemplo comprar una lista de productos o realizar siempre el mismo comentario puntuando con 4/5.

#### StandardClient:
Es la otra subclase que hereda de Client, al igual que la anterior también otorga características únicas a al cliente, en este caso estándar, tambíén durante la compra y realización de comentarios. En este tipo de clientes, la compra se realiza siempre en forma de dos pedidos con los dos productos más caros de la lista y los comentarios dependen de los parámetros del producto a comentar como la longitud del nombre.
 
### Comment:
 Esta clase se encarga de crear un comentario realizado por los clientes, así como de devolver dicho comentario, el producto al cual se hizo, su autor y la puntuación.

### DProduct:
 Es la clase principal del proyecto, sólo crea un objeto de tipo StockDemo y ejecuta las demos. También hace un intento de leer un fichero de configuración inicial si hay argumentos(nombre del fichero), en caso contrario lanza la GUI.
 
### Product:
 Gestiona la creación del producto, así como la devolución de los párametros del mismo, y las operaciones relacionadas con la compra, ya sea de una unidad o una cantidad determinada.

#### Home:
Subclase de Product referente a los productos de tipo hogar, tienen un descuento aplicado del 5%,puedes ser comentados y puntuados(like/dislike), son proveídos de un campo adicional que indica la parte de la casa para el que están dirigidos los productos

#### Entertainment:
Subclase de Product referente a productos de entretenimiento y ocio, no pueden ser puntuados(like/dislike), pero pueden ser comentados, no tienen descuento, pero se les aplica el 20% de IVA.

#### Food:
Subclase de Product referente a productos de alimentación, pueden ser puntuados(like/dislike) pero no pueden ser comentados, tienen un campo adicional que indica el mes en el que caducan los productos, tienen un descuento del 10%.

 
### StockDemo:
Clase que se encarga de ejecutar los turnos de los clientes.
 
### StockManager:
 Esta clase se encarga de gestionar el "stock" de la tienda, añadiendo productos al almacén, consultando el número almacenado de un producto,
 buscando si existe en el almacén y gestionando los envios.

## Interfaces:
Las interfaces del proyecto son usadas solo para las subclases de producto.

### Likeable:
Habilita al producto para ser puntuado mediante los métodos de like,dislike y getLikes.

### Commentable:
Permite que el producto pueda ser comentado por un cliente, el único método que provee es el de comment().
 
# Como poner en funcionamiento

Para hacer funcionar este proyecto hay que tener instalado JVM, que viene por defecto en el paquete JRE de Java.
Dado que el proyecto no viene empaquetado(.jar o .war) hay que ejectar directamente la clase principal(DProducts.class) encontrada en la ruta `/out/production/dproducts/DProducts.class` desde la ruta del proyecto con el comando `java DProducts`.
EC2-> Ahora el proyecto viene empaquetado en .jar, por lo que puede ejecutarse mediante doble click sobre el archivo o mediante línea de comandos `java -jar DProducts.jar`

# Problemas y soluciones

Para problemas, sugerencias y posibles soluciones envíe un Merge Request a este repositorio.

