# ---------------DProducts--------------

# Información:

## Descripción del proyecto:
Este proyecto busca imitar el funcionamiento de una empresa que se dedica a la venta de productos, similar a las tiendas online como "Amazon". Las operaciones más importantes son los pedidos de productos por parte de la compañía(StockManager) y de los clientes del mismo, junto con un constante intento por mantener un stock mínimo.
Los clientes por su parte piden productos a un solo StockManager y que a su vez estén en su lista de favoritos, pueden crear comentarios acerca de los productos que tiene entre sus favoritos.

## Clases:

### Client:
 Toma el papel de cliente(con sus correspondientes parámetros) y la asignación del mismo a un "StockManager",
 sus métodos están dedicados al pedido de los productos a la tienda, guardarlos como favorito o crear y guardar un comentario sobre  un producto.

### Comment:
 Esta clase se encarga de crear un comentario realizado por los clientes, así como de devolver dicho comentario, el producto al cual se hizo, su autor y la puntuación.

### DProduct:
 Es la clase principal del proyecto, sólo crea un objeto de tipo StockDemo y ejecuta las demos.

### Product:
 Gestiona la creación del producto, así como la devolución de los párametros del mismo, y las operaciones relacionadas con la compra,
 ya sea de uno o varios, de los productos.

### StockDemo:
 Realización de los ejercicios propuestos para la "demo de Sww", propuesta a última hora.

### StockManager:
 Esta clase se encarga de gestionar el "stock" de la tienda, añadiendo productos al almacén, consultando el número almacenado de un producto,
 buscando si existe en el almacén y gestionando los envios.

# Como poner en funcionamiento

Para hacer funcionar este proyecto hay que tener instalado JVM, que viene por defecto en el paquete JRE de Java.
Dado que el proyecto no viene empaquetado(.jar o .war) hay que ejectar directamente la clase principal(DProducts.class) encontrada en la ruta `/out/production/dproducts/DProducts.class` desde la ruta del proyecto con el comando `java DProducts`.

# Problemas y soluciones

Para problemas, sugerencias y posibles soluciones envíe un Merge Request a este repositorio.

